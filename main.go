package main

import (
	"fmt"
	"log"
	"os"
	"regexp"
	"strings"

	"github.com/joho/godotenv"
	"github.com/xanzy/go-gitlab"
)

var gitlabUser string = "cheesyhypocrisy"
var gitlabAddress string = "https://gitlab.com"
var matchRegex string = "foo.com/(.*)"

func handleError(err error, message string) {
	if err != nil {
		log.Fatalf("%s: %v", message, err)
	}
}

func main() {
	err := godotenv.Load()
	handleError(err, "Error loading .env file")
	gitlabToken := os.Getenv("GITLAB_ACCESS_TOKEN")

	git, err := gitlab.NewClient(gitlabToken, gitlab.WithBaseURL(gitlabAddress))
	handleError(err, "Failed to create client")

	projectOpts := &gitlab.ListProjectsOptions{ListOptions: gitlab.ListOptions{Page: 1, PerPage: 50}}
	_, resp, err := git.Projects.ListProjects(projectOpts)
	handleError(err, "Failed to list projects")

	for currPage := 1; currPage < resp.TotalPages; currPage++ {
		projectOpts := &gitlab.ListProjectsOptions{ListOptions: gitlab.ListOptions{Page: currPage, PerPage: 50}}
		projects, _, err := git.Projects.ListProjects(projectOpts)
		handleError(err, "Failed to list projects")

		for _, project := range projects {
            _, resp, err := git.Repositories.ListTree(project.ID, &gitlab.ListTreeOptions{ListOptions: gitlab.ListOptions{Page: 1, PerPage: 10}})
			if err != nil {
				fmt.Fprintln(os.Stderr, project.Name)
				continue
			}
            for currNode := 1; currNode < resp.TotalPages; currNode++ {
                treeNodes, _, err := git.Repositories.ListTree(project.ID, &gitlab.ListTreeOptions{ListOptions: gitlab.ListOptions{Page: currNode, PerPage: 10}})
                if err != nil {
                    fmt.Fprintln(os.Stderr, project.Name)
                    continue
                }
                for _, treeNode := range treeNodes {
                    if strings.Contains(treeNode.Path, "Dockerfile") {
                        fileBytes, _, err := git.RepositoryFiles.GetRawFile(project.ID, treeNode.Path, &gitlab.GetRawFileOptions{})
                        if err != nil {
                            fmt.Fprintln(os.Stderr, project.Name)
                            continue
                        }
                        re := regexp.MustCompile(matchRegex)
                        matchedString := re.Find(fileBytes)
                        if string(matchedString) != "" {
                            fmt.Printf("(%s)%s: %s\n", project.Name, treeNode.Path, string(matchedString))
                        }
                    }
                }
            }
		}
	}
}
